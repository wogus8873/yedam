package com.yedam.test;

public class SimplePayment implements Payment{
	//필드
		public double simplePaymentRatio;
		//생성자
		public SimplePayment(double simplePaymentRatio) {
			this.simplePaymentRatio = simplePaymentRatio; 
		}
		//메소드
		@Override
		public int online(int price) {
			int on = (int)(price - (price * (simplePaymentRatio+ONLINE_PAYMENT_RATIO)));
			return on;
		}
		@Override
		public int offline(int price) {
			int off = (int)(price - (price * (simplePaymentRatio+OFFLINE_PAYMENT_RATIO)));
			return off;
		}
		@Override
		public void showInfo() {
			System.out.println("***간편결제 시 할인정보");
			System.out.println("온라인 결제시 총 할인율 : "+(simplePaymentRatio+ONLINE_PAYMENT_RATIO));
			System.out.println("오프라인 결제시 총 할인율 :"+(simplePaymentRatio+OFFLINE_PAYMENT_RATIO));
		}
		
}
