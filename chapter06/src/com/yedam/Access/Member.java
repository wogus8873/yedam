package com.yedam.Access;

public class Member {
	//필ㄹ드
	private String id;
	private String pw;
	private String name;
	private int age;  //나이는 음수가 될 수 없음  음수가 안 들어가게 방지하는게 setter getter
	
	
	
	//private로 접근을 막음 클래스로 객체 만들때
	//막으면 어떻게 값을주나 
	//밑에서 세터로 age에 값을 입력하고 음수가 안 들어오게 설정 하는것.
	
	//생성자 
	
	//메소드
	//getter setter 사용 데이터의 무결성을 지키기위해
	public void setAge(int age) {
		if(age < 0) {
			System.out.println("잘못된 나이입니다");
//			this.age = 0;  //return 이나 이 줄 둘중하나 쓰면 될듯
			return;   //return; -> return; 을 만나면 하던것 멈추고 메소드 종료후 메소드 호출한곳으로 이동
		}else {
			this.age = age;
		}
		System.out.println("return적용안됨");
	}
	
	public int getAge() {
		//미국의 나이와 우리나라의 나이는 한살 차이 
		//아래내용으로 추가한다
		age = age+1;
		return age;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		if(id.length()<=8) {    //id.length() 는 문자열의 길이
 			System.out.println("8자 미만입니다 다시 입력 ㄱ");
			return;
		}
		this.id = id;
	}

	public String getPw() {
		return pw;
	}

	public void setPw(String pw) {
		this.pw = pw;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
	
	
	
	
	
	
}
