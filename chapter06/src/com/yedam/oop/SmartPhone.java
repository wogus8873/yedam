package com.yedam.oop;

public class SmartPhone {
	//필드 
	//객채의 정보를 저장
	String name;
	String maker;
	int price;
	
	
	
	//생성자(클래스 이름과 똑같이 부여해서 만듦)
	//자바에서 생성자가 클래스 내부에 "하나도 없을때" 알아서 기본 생성자를 
	//만들고 객체 생성.
	public SmartPhone(){
		
	}
	
	public SmartPhone(String name) {
		this.name = "iphone14Pro";
	}
	
	public SmartPhone(int price) {
		
	}
	public SmartPhone(String name, int price) {
		//객체를 만들때 내가 원하는 행동 또는 데이터 저장 등등
		//할때 여기에 내용 구현
	}
	
	public SmartPhone(String maker , String name, int price) {
			this.name = name;  // 매개변수(new SmartPhone(name,2,3))로 name 을 받아서 this.로name을 필드내에 name으로 전달?
			this.maker = maker;													 //this 는 내 자신  = 클래스를 말함 => 클래스의 네임 
			this.price = price;   
			
	}
	
	
	
	//메소드
	//객체의 기능을 정의
	void call() {
		System.out.println(name + "전화를 겂니다.");
	}
	void hangUp() {
		System.out.println(name + "전화를 끊습니다.");
	}
	
	//1) 리턴 타입이 없는 경우: void
	void getInfo(int no) {
		System.out.println("매개변수"+no);
	}
	
	//2)리턴타입이 있는 경우
	//  1. 기본타입 : int double long ... 등등
	//  2. 참조타입 : String 배열 클래스 ... 등등 
	//2-1) 리턴타입이 기본타입일경우
	int getInfo(String temp) { 
		
		return 0;  //리턴타입이라 리턴이 꼭 필요. 리턴타입이 int 라 0 String 이면 null이겠지,,
	}
	
	//2-2)리턴타입이 참조 타입일 경우
	String[] getInfo(String[] temp) {
		
		return temp;   //스트링타입의 배열 배열 타입 마춰준거
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
		
}
