package com.yedam.inter2;

public class Application {
	public static void main(String[] args) {
		
		Vehicle v1 = new Bus();
//		drive(v1);
//		
//		Vehicle v2 = new Taxi();
//		drive(v2);
		
		v1.run();
//		v1.checkFare();
		Bus bus1 = new Bus();
//		Taxi taxi = (Taxi)bus1; // 부모자식간에 되는거임 다른아들내미라 안돼
								//이걸 막아주는게 instanceof 
		
		Bus bus = (Bus)	v1;
		bus.checkFare();
		
		drive(new Bus());
		drive(new Taxi());
		
	}
	
	public static void drive(Vehicle vehicle) {
		
		if(vehicle instanceof Bus) {
			Bus bus = (Bus) vehicle;
			bus.checkFare();
		}
		
		vehicle.run();
	}
}
