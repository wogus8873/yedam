package com.yedam.inter2;

public class InterFaceEx {
	public static void main(String[] args) {
		ImplementC impl = new ImplementC();
		
		InterFaceA ia = impl;
		ia.methodA(); 
		
		System.out.println();
		
		InterFaceB ib = impl;
		ib.methodB();
		
		System.out.println();
		
		InterFaceC ic = impl;
		ic.methodC();
		ic.methodB();
		ic.methodA();
	}
}
