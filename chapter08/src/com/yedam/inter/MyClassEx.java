package com.yedam.inter;

public class MyClassEx {
	public static void main(String[] args) {
		System.out.println("1===============");
		
		//마이클래스 안에 rc부름 rc는 리모트로만들어진 텔레비전(오버라이딩)
		//마이클래스 알씨 가 가지고있는 턴온을 실행해라 그래서 .이 두개
		//배열이라고보면 배열의 몇번째방의 그것가져와 
		MyClass myclass = new MyClass();
		myclass.rc.turnOn();
		myclass.rc.turnOff();
		
		System.out.println("2==============");
		
		MyClass myclass2 = new MyClass(new Audio()); //Audio 오버라이딩 돼서 옴
		
		
		System.out.println("3==============");
		
		MyClass myclass3 = new MyClass();
		myclass3.method1();
		
		System.out.println("4==============");
		
		MyClass myclass4 = new MyClass();
		
		myclass4.methodB(new Television());
		
		
		
		
		
		
		
		
		
		
		
		
	}
}
