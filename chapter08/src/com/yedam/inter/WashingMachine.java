package com.yedam.inter;

public interface WashingMachine extends DryCourse {
	public void startBtn();
	public void pauseBtn();
	public void stopBtn();
	public int changeSpeed(int speed);
}
