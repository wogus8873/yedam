package com.yedam.API;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateEx {
	public static void main(String[] args) {
		//날짜 가져오기위해 Date 클래스 호출
		Date now = new Date();
		System.out.println(now.toString());  //toString() 은 문자정보 가져오는것 
													//다 소문자지만 m은 중복이라 월은 M으로 함
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy년 MM월 dd일 hh시 mm분 ss초");
		System.out.println(sdf.format(now));
		
//		SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy년 MM월 dd일 hh시");
//		System.out.println(sdf1.format(now));
		
		
		
		//Calender클래스
		
		Calendar now2 = Calendar.getInstance();  // 추상클래스라서 new로 객체생성 불가 그래서 getInstance 사용
		
		int year = now2.get(Calendar.YEAR);
		int month = now2.get(Calendar.MONTH)+1;  //1월은 0로 시작해서 +1해줌
		int day = now2.get(Calendar.DAY_OF_MONTH);
		
		int week = now2.get(Calendar.DAY_OF_WEEK);
		
		String strWeek = "";
		
		switch (week) {
		case Calendar.MONDAY:
			strWeek = "월";
			break;
		case Calendar.TUESDAY:
			strWeek = "화";
			break;
		case Calendar.WEDNESDAY:
			strWeek = "수";
			break;
		case Calendar.THURSDAY:
			strWeek = "목";
			break;
		case Calendar.FRIDAY:
			strWeek = "금";
			break;
		case Calendar.SATURDAY:
			strWeek = "토";
			break;
		case Calendar.SUNDAY:
			strWeek = "일";
			break;
		}
		
		
		int amPm = now2.get(Calendar.AM_PM);
		String strAmpm = "";
		
		if(amPm == Calendar.AM) {
			strAmpm = "오전";
		}else if(amPm == Calendar.PM) {
			strAmpm = "오후";
		}
		
		int hour = now2.get(Calendar.HOUR);
		int minute = now2.get(Calendar.MINUTE);
		int sec = now2.get(Calendar.SECOND);
		
		System.out.println(year);
		System.out.println(month);
		System.out.println(day);
		System.out.println(strWeek);
		System.out.println(strAmpm);
		System.out.println(hour);
		System.out.println(minute);
		System.out.println(sec);
		
		
		
		
		
		
	}
}
