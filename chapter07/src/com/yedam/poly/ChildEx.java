package com.yedam.poly;

public class ChildEx {
	public static void main(String[] args) {
//		Child child = new Child();
//		
//		Parent parent = child;  //child에 메소드3이 있다고해도 실행 안됨.
//		
//		//클래스간의 자동 타입변환
//		//부모클래스에 있는 메소드를 사용하되 
//		//단, 자식클래스에 재정의가 돼있으면 자식클래스에 재정의된 메소드를 사용한다.
//		
//		//parent 는 메소드1,2만 가지고있어서 메소드3 실행 불가
//		parent.method1();  //parent가 가진 메소드 출력
//		parent.method2();  //child 에 오버라이딩 된 메소드 출력 
////		parent.method3();  //parent 에 method3은 없음 그래서 오류
		
	//클래스간의강제 타입 변환
		//자동타입변환으로 인해서 자식클래스 내부에 정의된 필드, 메소드를 못 쓸경우
		//강제타입변환을 함으로써 자식클래스 내부에 정의된 필드와 메소드 사용.
		
		Parent parent = new Child();
		
		parent.field  ="data1";
		parent.method1();
		parent.method2();
//		parent.field2 = "data2";
//		parent.method3();
		
		Child child = (Child) parent;
		child.field2 = "data2";
		child.method3();
		child.method1();
		child.method2();
		child.field = "data";
		
		//클래스 타입 확인 예제
		
		
		
		method1(new Parent());
		method1(new Child());
		
		GrandParent gp = new Child();
		//G<-P<-C
		gp.method4();
		
		
	
	}
	public static void method1(Parent parent) {
		if(parent instanceof Child) {
			Child child = (Child) parent;
			System.out.println("변환성공");
		}else {
			System.out.println("변환실패");
		}
	}
}
