package com.yedam.inheri;

public class OverrideEx {
	public static void main(String[] args) {
		Child child = new Child();
		
		child.method1();  //덮어씀  자식의 메소드가 출력된것
		child.method2();  //안덮어씌여짐  자식클래스에서 메소드2를 정의하지않음
		child.method3();  //아예 새로 생성한것   부모에게 없는 메소드.
		
		//부모의 메소드가 맘에 안 들면 자식클래스에서 덮어씌우면 됨.
		
	}
}
