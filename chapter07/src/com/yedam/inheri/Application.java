package com.yedam.inheri;

public class Application {
	public static void main(String[] args) {
		Child child = new Child();
		
		child.lastName = "똬치";
		child.age = 20;
		
		System.out.println("내이름은 : "+ child.firstName + child.lastName); //firstName 부모 , lastName 자식 
		System.out.println("DNA : "+child.DNA); //DNA 부모
		//Parent 클래스 -> bloodType 을 private 설정 해서 
		//Child 클래스 ->  Parent 클래스의 bloodType 사용 못 함 
		System.out.println("혈액형 : " + child.bloodType); //bloodType 부모 
		System.out.println("나이 : " + child.age);//age 자식 
		
		
		Child2 child2 = new Child2();
		
		child2.lastName = "희동";
		child2.age = 5;
		child2.bloodType = 'A'; //따로 설정한 혈액형 정보는 바뀌지 않았음.
		
		
		System.out.println("내이름은 : "+ child2.firstName + child2.lastName); //firstName 부모 , lastName 자식 
		System.out.println("DNA : "+child2.DNA); //DNA 부모
		//Child 에 존재하는 bloodType 이라 상관 없음
		System.out.println("혈액형 : " + child2.bloodType); //bloodType 자식
		System.out.println("나이 : " + child2.age);//age 자식 
		
		
		
		
		
		
		
		
		
		
	}
}
