package com.yedam.inheri;

public class SuperSonicAirPlane extends AirPlane {
	//필드
	//일반비행
	public static final int NORMAR = 1;
	//초고속비행
	public static final int SUPERSONIC = 2;
	
	public int flyMode = NORMAR;
	//셍성자

	
	
	//메소드
	@Override
	public void fly() {
		if(flyMode == SUPERSONIC) {
			System.out.println("초음속 비행모드");
		}else {
			super.fly(); //부모클래스의 fly를 가져와서 슴
		}
	}
	
}
