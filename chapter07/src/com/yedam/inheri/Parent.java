package com.yedam.inheri;

public class Parent {
	//부모클래스
	//부모의 정보를 바꾸면 자식도 따라 바뀜
	//1)상속 할 필드 정의
	protected String firstName = "고";
	public char bloodType = 'B';
	protected String DNA = "B";
	//2)상속 대상에서 제외 
	protected String lastName;
	public int age;
	
	
	
	//메소드
	//오버라이딩 예제
	protected  void method1() {
		System.out.println("parent class -> method1 Call");
	}
	
	public void method2() {
		System.out.println("parent class -> method2 Call");
	}
	
	
	
	
	
	
	
}
