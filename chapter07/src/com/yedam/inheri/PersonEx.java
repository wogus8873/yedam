package com.yedam.inheri;

public class PersonEx {
	public static void main(String[] args) {
		Person p1 = new Person("고희동","123456-123124");
		
		//부모클래스의 필드호출
		//Person()을 실행할 때 부모객체를 만들어서
		//그안에 있는 인스턴스 필드를 가져와서 실행
		System.out.println("name :"+p1.name);
		System.out.println("ssn : "+p1.ssn);
		
		
		//Person()을 실행할때 자신이 가지고있는 age를 가져와
		//
		System.out.println("age : "+p1.age);
		
		
		
		
		
		
		
		
		
		
	}
}
