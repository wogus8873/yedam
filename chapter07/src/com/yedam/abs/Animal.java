package com.yedam.abs;

public abstract class Animal {
	
	public String kind;
	
	public void breathe() {
		System.out.println("숨쉼");
	}
	
	//추상메소드
	//상속받게되면 무조건 오버라이딩해야함
	
	public abstract void sound();  //꼭 만들어야할 기능 
	
	
}
