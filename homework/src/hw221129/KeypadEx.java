package hw221129;

public class KeypadEx {
	public static void main(String[] args) {
		Keypad keypad = new RPGgame();
		
		keypad.leftUpButton();
		keypad.rightUpButton();
		keypad.changeMode();
		keypad.rightUpButton();
		keypad.rightDownButton();
		keypad.leftDownButton();
		keypad.changeMode();
		keypad.rightDownButton();
		
		System.out.println("=============================");
		
		Keypad keypad2 = new ArcadeGame();
		
		keypad2.leftUpButton();
		keypad2.rightUpButton();
		keypad2.leftDownButton();
		keypad2.changeMode();
		keypad2.rightUpButton();
		keypad2.leftUpButton();
		keypad2.rightDownButton();
		
	}
}
