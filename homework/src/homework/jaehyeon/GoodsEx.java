package homework.jaehyeon;

import java.util.Scanner;

public class GoodsEx {
	public static void main(String[] args) {
		// 문제2) 다음은 키보드로부터 상품 수와 상품 가격을 입력받아서
		// 최고 가격을 가지는 제품과 해당 제품을 제외한 제품들의 총 합을 구하는 프로그램을 작성하세요.
		// 1) 메뉴는 다음과 같이 구성하세요.
		// 1.상품 수 | 2.상품 및 가격입력 | 3.제품별 가격 | 4.분석 | 5.종료
		// 2) 입력한 상품 수만큼 상품명과 해당 가격을 입력받을 수 있도록 구현하세요.
		// 3) 제품별 가격을 출력하세요.
		//	출력예시, "상품명 : 가격"
		// 4) 분석기능은 최고 가격을 가지는 제품과 해당 제품을 제외한 제품들의 총합을 구합니다.
		// 5) 종료 시에는 프로그램을 종료한다고 메세지를 출력하도록 구현하세요.
		Scanner sc = new Scanner(System.in);
		int goodsCount = 0;
		Goods[] goodsAry = null;
		
		
		while(true) {
			System.out.println("1.상품 수 |2.상품 및 가격입력 |3.제품별 가격 |4.분석 |5종료");
			System.out.println("입력(1~5) :");
			int selectNum = Integer.parseInt(sc.nextLine());
			
			if(selectNum == 1) {
				System.out.println("상품수를 입력하세요 :");
				goodsCount = Integer.parseInt(sc.nextLine());//배열의 크기를 받는 행동만
														//배열의 크기를 받자마자 확정하기보단 상품값을 입력할때 배열의 값을 정해주는게 안정적이다.
			}else if(selectNum==2) {
				goodsAry = new Goods[goodsCount];	//배열의 크기 받은것을 확정
				for(int i=0; i<goodsAry.length; i++) {
					Goods goods = new Goods();  //밖에다 쓰며는 하나의 상품에 덮어쓰는게 돼버림(새로운 하나의 객체에 정보 덮어씀) 그래서 안에다가 쓰는거 안에다가 계속 새로운 객체 만드는거
					
					System.out.println("상품 이름 입력하세요 :");
					String name = sc.nextLine();
					System.out.println("상품의 가격을 입력하세요 :");
					int price = Integer.parseInt(sc.nextLine());
					
					goods.goodsName = name;
					goods.goodsPrice = price; 
					
					goodsAry[i] = goods;
				}
				
			}else if(selectNum==3) {
				for(int i=0; i<goodsAry.length; i++) {
					System.out.println(goodsAry[i].goodsName+" : "+goodsAry[i].goodsPrice);
				}
	
			}else if(selectNum==4) {
				int max = goodsAry[0].goodsPrice;
//				int other = 0;//내가한 방법
				int sum = 0;
				for(int i=0; i<goodsAry.length; i++) {
					if(max < goodsAry[i].goodsPrice) {
						max = goodsAry[i].goodsPrice;
					}
					sum += goodsAry[i].goodsPrice;
				}
//				for(int i=0; i<goodsAry.length; i++) {    //내가한 방법
//					if(max != goodsAry[i].goodsPrice) {
//						other += goodsAry[i].goodsPrice;
//					}
//				}
				
				
				System.out.println("최고가 : "+max);
//				System.out.println("나머지의 합 :"+other); //내가한 방법
				System.out.println("최고가를 제외한 나머지 합 : "+(sum-max));
				
				
			}else if(selectNum==5) {
				System.out.println("프로그램 종료");
				break;
			}
			
			
			
			
			
			
		}
		
		
	
		
	}
}
