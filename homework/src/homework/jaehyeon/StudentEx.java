package homework.jaehyeon;

public class StudentEx {
	public static void main(String[] args) {
		Student std1 = new Student();
		
		std1.setName("김또치");
		std1.setDap("컴퓨터공학과");
		std1.setGrade(2);
		std1.setProgramSco(50);
		std1.setDbSco(60);
		std1.setOsSco(90);
		
		
		System.out.println("이름 : "+ std1.getName());
		System.out.println("학과 : "+ std1.getDap());
		System.out.println("학년 : "+ std1.getGrade());
		System.out.println("프로그래밍 점수 : "+ std1.getProgramSco());
		System.out.println("데이터 베이스 점수 : "+std1.getDbSco());
		System.out.println("오에스 점수 : "+ std1.getOsSco());
		
	}
}
