package homework.jaehyeon;

public abstract class Culture {
	public String title;
	public int director;
	public int actor;
	public int audience=0;
	public int total=0;
	
	
	public Culture(String title,int director,int actor) {
		this.title = title;
		this.director = director;
		this.actor = actor;
	}
	
	//메소드
	
	public void setTotalScore(int score) {//관객수와 총점을 누적시키는 기능
		//관객1씩증가
		this.audience++;
		//점수누적(총점)
		this.total+=score;
	}
	
	public String getGrade() {//평점을 구하는 기능
		int avg = total/audience;
		
		String grade = null;
		
		switch(avg){
			case 1:
				grade = "★";
				break;
			case 2:
				grade = "★★";
				break;
			case 3:
				grade = "★★★";
				break;
			case 4:
				grade = "★★★★";
				break;
			case 5:
				grade = "★★★★★";
				break;
		}
		//다른방법
		//for(int i=0; i<avg; i++){ 
		//	
		//}
		return grade;
		
	}
	public abstract void getInformation(); //: 정보를 출력하는 추상메소드
		
	
	
}
