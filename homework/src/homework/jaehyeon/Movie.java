package homework.jaehyeon;

public class Movie  extends Culture{
	//필드
	public String genre;
	
	
	//생성자
	public Movie(String name,int director,int actor,String genre) {
		super(name,director,actor);
		this.genre = genre;
		
	}
	
	//메소드
	public void getInformation() {
		//제목, 참여감독 수, 참여배우 수, 관객수, 총점, 평점을 출력하는 기능
		System.out.println(genre+"제목 : "+ title);
		System.out.println(genre+"감독수 : "+ director);
		System.out.println(genre+"배우수 :"+actor);
		System.out.println(genre+"총점 : "+total);
		System.out.println(genre+"총점 : "+getGrade());
		
	}

}