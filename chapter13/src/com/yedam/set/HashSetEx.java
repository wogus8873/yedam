package com.yedam.set;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class HashSetEx {
	public static void main(String[] args) {
		
	Set<String> set = new HashSet<>();
	
	//String str = "Java";
	//String str2 = "Java" 는 같은것
	
	set.add("Java");
	set.add("JDBC");
	set.add("Servlet/JSP");
	set.add("Java");      
	set.add("iBatis");
	
	int size = set.size();
	System.out.println("총 객체수 : "+size); //4개로뜸
	
	Iterator<String> iterator = set.iterator();
	
	
	//반복자
	while(iterator.hasNext()) {  //hasNext == 참 거짓 가져올게있으면 참 /가져올게 없으면 거짓
		String elemet = iterator.next();
		System.out.println("\t"+ elemet);
	}
	System.out.println();
	
	set.remove("Java");
	set.remove("JDBC");
	
	
	//향상된 for
	for(String temp : set) {  //객체 만들때 String 타입이라 String 
		System.out.println("\t"+temp);
	}
	
	set.clear();
	System.out.println("총 객체 수 : "+set.size());
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	}
}
