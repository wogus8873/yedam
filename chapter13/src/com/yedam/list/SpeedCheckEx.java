package com.yedam.list;

import java.util.LinkedList;
import java.util.List;

public class SpeedCheckEx {
	public static void main(String[] args) {
		List<String> aList = new LinkedList<>();
		List<String> lList = new LinkedList<>();
		
		long strTime;
		long endTime;
		
		strTime = System.nanoTime();
		for(int i=0; i<100000; i++) {
			aList.add(0,String.valueOf(i));
			
		}
		endTime = System.nanoTime();
		
		System.out.println("ArrayList 걸린시간 : "+(endTime-strTime)+"ns");
		
		strTime = System.nanoTime();
		for(int i=0; i<100000; i++) {
			lList.add(0,String.valueOf(i));
			
		}
		endTime = System.nanoTime();
		
		System.out.println("LinkedList 걸린시간 : "+(endTime-strTime)+"ns");
	}
}
