package com.yedam.list;

import java.util.ArrayList;
import java.util.List;

public class listEx {
	public static void main(String[] args) {
		//List<E>
		List<String> list = new ArrayList<String>();
		
		list.add("java");  //인덱스가 0인곳에 추가
		list.add("JDBC");  //인덱스가 1인곳에 추가
		list.add("Servlet/JSP");//인덱스가 2인곳에 추가
		list.add(2,"DataBase");//인덱스가 2인곳에 추가 -> JSP가 3으로 밀리고 database가 인덱스2에 추가  
								//인덱스 0 - 자바 /1 - JDBC/ 2 - DataBse /3 - JSP
		list.add("iBATIS");//인덱스가 4인곳에 추가
		
		//list 크기 , length == size
		int size = list.size();
		System.out.println("총 객체 수 : "+size);
		System.out.println();
		
		//list객체 가져오기
		String skill =  list.get(2); //인덱스가 2인곳에 데이터 가져와
		System.out.println("index 2 : "+skill);
		System.out.println();
		
		//list 크기만큼 반복문 돌리기
		for(int i=0; i<list.size(); i++) {
			String str = list.get(i);
			System.out.println(i+" : "+ str);
					
		}
		System.out.println();
		
		list.remove(2); //DataBase 삭제
						//빈공간을 뒤에꺼가 채워줌 그래서 Servlet 이 (2)
		for(int i=0; i<list.size(); i++) {
			String str = list.get(i);
			System.out.println(i+" : "+ str);
					
		}
		System.out.println();
		
		list.remove("JDBC"); //객체를 찾아서 지움
		
		for(int i=0; i<list.size(); i++) {
			String str = list.get(i);
			System.out.println(i+" : "+ str);
					
		}
		System.out.println();
		
		
		
	}
}
