package com.yedam.map;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class HashMapEx {
	public static void main(String[] args) {
		Map<String,Integer> map = new HashMap<String,Integer>();
		
		
		//객체의저장
		map.put("홍길동", 85);
		map.put("김또치", 90);
		map.put("고희동", 80);
		map.put("김또치", 33);//덮어씀 그래서 3개출럭
		System.out.println("총 Entry 수 : "+ map.size());
		
		//객체의 찾기
		System.out.println(map.get("김또치"));
		System.out.println();
		
		//객체를 하나씩 처리
		Set<String> keySet = map.keySet();
		Iterator<String> keyIterator = keySet.iterator();
		while(keyIterator.hasNext()) {
			String key = keyIterator.next();
			Integer value = map.get(key);
			System.out.println("key : "+key+"value : "+value);

		}
		
		//객체 삭제
		map.remove("홍길동");
		System.out.println("총 Entry 수 : "+map.size());
		
		
		//객체 하나씩 처리
		Set<Map.Entry<String,Integer>>entryset = map.entrySet();//key value 다 가져옴
		Iterator<Map.Entry<String, Integer>> entryIterator = entryset.iterator();  //다 넣어줌 반복
		
		while(entryIterator.hasNext()) {
			Map.Entry<String,Integer> entry = entryIterator.next();
			String key = entry.getKey();
			Integer value = entry.getValue();
			System.out.println("key: "+key+"value: "+value);
		}
		System.out.println();
		
		//객체 전체 삭제 
		map.clear();
		System.out.println("총 Entry 수 : "+map.size());
		
		
		
		
		
		
		
		
		
		
		
	}
}
