package com.yedam.common;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

public class DAO {
	//DAO -> DATA ACCESS OBJECT
	//JAVA -> DB 연결해주는 객체(JDBC)
	//OJDBC를 가져와서 자바프로젝트에 추가 
	
	//java -> DB 연결할 때 쓰는 객체
	protected Connection conn = null;
	//java.sql임포트 
	
	//Select(조회) 결과 값 반환 받는 객체 
	protected ResultSet rs = null;
	
	//Query문 을 가지고 실행하는 객체
	protected PreparedStatement pstmt = null;
	//Query문 을 가지고 실행하는 객체
	protected Statement stmt = null;
	
	//SELECT , INSERT , UPDATE , DELETE 등등의 Query 문을
	//DB로 가져가서 실행시킴.
	//Ex) SELECT * FROM employees;
	
	//접속시도(접속정보)
	String driver = "oracle.jdbc.driver.OracleDriver";
								   //IP      :PORT:DBname
	String url = "jdbc:oracle:thin:@localhost:1521:xe";
	String id = "hr";
	String pw = "hr";
	
	//DB연결 메소드
	public void conn() {
		
		try {
		//1. 드라이버 로딩
		Class.forName(driver);
		//2. DB 연결
		
		conn = DriverManager.getConnection(url,id,pw);
		}catch(Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public void disconn() {
		try {
			if(rs != null) {
				rs.close();
			}
			if(stmt != null) {
				stmt.close();
			}
			if(pstmt != null) {
				pstmt.close();
			}
			if(conn != null) {
				conn.close();
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	
	
	
	
}
