package com.yedam.exe;

import java.util.Scanner;

import com.yedam.student.StudentService;

public class ExeApp {
	Scanner sc = new Scanner(System.in);
	StudentService ss = new StudentService();
	
	public ExeApp() {
		run();
	}
	
	
	private void run() {
		String menu = "";
		while(true) {
			System.out.println("1.학생정보조회 | 2.학생등록 | 3.학생퇴학 | 4.학생전공변경 | 5.종료");
			menu = sc.nextLine();
			if(menu.equals("1")) {
				
				ss.getStudentList();
				
			}else if(menu.equals("2")) {
				
				ss.insertStudent();
				
			}else if(menu.equals("3")) {
				
				ss.deleteStudent();
				
			}else if(menu.equals("4")) {
				
				ss.updateStudent();
				
			}else if(menu.equals("5")) {
				System.out.println("오와리다.");
				break;
			}
			
			
			
			
			
		}
		
	}
	
	
}
