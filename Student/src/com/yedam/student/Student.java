package com.yedam.student;

public class Student {
//	이름        널?       유형           
//			--------- -------- ------------ 
//			STD_ID    NOT NULL NUMBER(8)    
//			STD_NAME  NOT NULL VARCHAR2(15) 
//			STD_MAJOR          VARCHAR2(30) 
//			STD_POINT          NUMBER(2,1)
	
	private int stdId;
	private String stdName;
	private String stdMahor;
	private double stdPoint;
	
	
	
	public int getStdId() {
		return stdId;
	}
	public void setStdId(int stdId) {
		this.stdId = stdId;
	}
	public String getStdName() {
		return stdName;
	}
	public void setStdName(String stdName) {
		this.stdName = stdName;
	}
	public String getStdMahor() {
		return stdMahor;
	}
	public void setStdMahor(String stdMahor) {
		this.stdMahor = stdMahor;
	}
	public double getStdPoint() {
		return stdPoint;
	}
	public void setStdPoint(double stdPoint) {
		this.stdPoint = stdPoint;
	}
	
	
	
}
