package com.yedam.student;

import java.util.List;
import java.util.Scanner;

public class StudentService {
	Scanner sc = new Scanner(System.in);
	//전체학생조회
	public void getStudentList() {
		List<Student> list = StudentDAO.getInstence().getStudentList();
		
		for(Student student : list) {
			System.out.println("학번 : "+student.getStdId());
			System.out.println("이름 : "+student.getStdName());
			System.out.println("전공 : "+student.getStdMahor());
			System.out.println("학점 : "+student.getStdPoint());
			System.out.println("==================================");
		}
		
	}
	//학생등록
	public void insertStudent() {
		Student student = new Student();
		int id = 0;
		String name = "";
		String major = "";
		double point = 0;
		
		System.out.println("학번 : ");
		id = Integer.parseInt(sc.nextLine());
		System.out.println("이름 : ");
		name = sc.nextLine();
		System.out.println("전공 : ");
		major = sc.nextLine();
		System.out.println("점수 : ");
		point = Double.parseDouble(sc.nextLine());
		
		student.setStdId(id);
		student.setStdName(name);
		student.setStdMahor(major);
		student.setStdPoint(point);
		
		int result = StudentDAO.getInstence().insertStudent(student);
		
		if(result >= 1) {
			System.out.println("등록됐심더");
		}else {
			System.out.println("등록 안됐는데예..");
		}
	}
	//학생퇴학
	public void deleteStudent() {
		System.out.println("퇴학시킬 학생 이름 : ");
		String name = sc.nextLine();
		
		int result = StudentDAO.getInstence().deleteStudent(name);
		
		if(result >= 1) {
			System.out.println(name+"컷.");
		}else {
			System.out.println("퇴학처리가 이루어지지 않았습니다.");
		}	
	}
	//전공변경
	public void updateStudent() {
		Student student = new Student();
		
		System.out.println("전공변경을 희망하는 학생 : ");
		student.setStdName(sc.nextLine());
		System.out.println("변경할 전공 : ");
		student.setStdMahor(sc.nextLine());
		
		int result = StudentDAO.getInstence().updateStudent(student);
		
		if(result >= 1) {
			System.out.println("변경 완료 ㅋㅋ");
		}else {
			System.out.println("변경 안 됐습니다.");
		}
		
	}
	
	
	
	
	
	
	
}
