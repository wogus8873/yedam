package com.yedam.array;

public class Exam01 {
	public static void main(String[] args) {
		int[] intArry = {1,2,3,4,5,6};
		//int{} intArry = new int[6];
		//intArry[0] = 1;
		//intArry[1] = 2; ....
		
		
		String[] strArry = new String[10];
		
		int[] intArry2;
		intArry2 = new int[5];
		
		
		
		int[] scores = {83,90,87};
		
		System.out.println("scores 첫번째 데이터 : "+scores[0]);
		System.out.println("scores 세번째 데이터 : "+scores[2]);
		
		//반복문과 배열
		int sum = 0;
		
		for(int i=0; i<3; i++) {  //0~2
			System.out.println(scores[i]);
			sum += scores[i];
		}
		System.out.println("총합계 : "+sum);
		
		
		
		// 1) new연산자를 활용해 배열 만들기             /위에꺼랑 같음
		int[] point;
		point = new int[] {83,90,87};
		
		sum = 0;
		for(int i=0; i<3; i++) {
			System.out.println(point[i]);
			sum += point[i];
		}
		System.out.println("총합계 : "+sum);
		
		
		// 2)
		int[] arr1 = new int[3];
		for(int i=0; i<3; i++) {
			System.out.println("arr1["+i+"] : "+arr1[i]);      // 값을 안 넣으면 디폴트 0 임.
		}
		
		arr1[0] = 10;
		arr1[1] = 20;
		arr1[2] = 30;
		
		for(int i=0; i<3; i++) {
			System.out.println("arr1["+i+"] : "+arr1[i]);      // 값을 넣어서 출력.
		}
		
		
		double[] arr2 = new double [3];
		
		for(int i=0; i<3; i++) {
			System.out.println("arr2["+i+"] : "+arr2[i]);      // 값을 안 넣으면 디폴트 0.0 임.
		}
		
		arr2[0] = 0.1;
		arr2[1] = 0.2;
		arr2[2] = 0.3;
		
		for(int i=0; i<3; i++) {
			System.out.println("arr2["+i+"] : "+arr2[i]);      // 값을 넣어서 출력.
		}
		
		
		
		
		
		String[] arr3 = new String[3];
		
		for(int i=0; i<3; i++) {
			System.out.println("arr3["+i+"] : "+arr3[i]);      //값 안 넣으면 디폴트 null
		}
		
		arr3[0] = "3월";
		arr3[1] = "11월";
		arr3[2] = "12월";
		
		for(int i=0; i<3; i++) {
			System.out.println("arr3["+i+"] : "+arr3[i]);      //값 넣고 출력 
		}
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	}
}
