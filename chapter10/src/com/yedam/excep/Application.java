package com.yedam.excep;

public class Application {
	public static void main(String[] args) {
//		try {
//			//예외가 발생할만한 코드
//		}catch() {
//			//예외 발생 후 처리하는 코드
//		}
		//1 ) 자바가 먼저 컴파일 후 인식해서 예외처리.
		
		try {
			
			double avg = 1/0;
			System.out.println(avg);  //정의하지 않은(밑에) 오류 
			
			String str = "자바"; 
			//NumberFormat 예외 발생
			Integer.parseInt(str);
			System.out.println("변환 완료");
			//ClassNotFound 예외 발생
			Class clazz = Class.forName("java.lang.String2");
			System.out.println("예외처리 발생");
		}  catch (ClassNotFoundException e) {  //Class 어쩌구 오류가 발생하면,,?
		
			e.printStackTrace(); //예외가 발생한 경로를 추적하고 console에 출력하세요 
			System.out.println("클래스 예외");
		} catch (NumberFormatException e) {
			e.printStackTrace();
			System.out.println("넘버 예외");
		}	catch(Exception e) {  //얘는 모든 예외를 확인가능해서 위에꺼두개 실행하고 나머지 쩌리 우리가 모르는걸 걸러줌 그래서 맨밑으로 해놔야함 그래야 위에꺼 오류가 안 남 이게 맨 위에 있으면 오류
			e.printStackTrace();
			System.out.println("Exception 발생");
			
		} finally {
			System.out.println("finally 실행");
		}
		
		System.out.println("try-catch 탈출");
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	}
}
