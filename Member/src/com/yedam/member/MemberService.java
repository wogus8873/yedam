package com.yedam.member;

import java.util.List;
import java.util.Scanner;

public class MemberService {
	Scanner sc = new Scanner(System.in);
	//member의 정보를 자바에 공유하기위해 static변수 선언
	//1.데이터가 들어있는경우 ->로그인이 돼있다.
	//2.데이터가 없어서null인 경우 ->로그인이 안 돼있다.혹은 로그아웃 하겠다
	public static Member memberInfo = null;
	
	//1.로그인
	public void login() {
		Member member = new Member();
		String id = "";
		String pw = "";	
		System.out.println("ID : ");
		id = sc.nextLine();
		System.out.println("PW : ");
		pw = sc.nextLine();
		
		//member는 id 만 가지고있는 객체
		member.setMemberId(id);//id가지고 멤버에 데이터 넣어줌
		
		//id를 통해서 회원의 정보를 조회하고 조회된 결과를 member객체에 넣어주는 부분
		//1.id를 통해 조회가 됐으면 -> 회원의 정보가 존재하고 입력한 id도 일치. member객체는 회원의 정보가 들어있음.
		//따라서 입력한 비밀번호와 조회한 비멀번호가 일치하면 로그인 시켜주는것.
		//2.id를 통해 조회 안 됨 -> 회원정보가 없다. 즉 member객체는 null
		member = MemberDAO.getInstance().login(member);//로그인시도 (id) memberDAO에 로그인 실행 (정보가 있거나 , null)
		//조회한 정보와 내가 입력한 정보가 같은지 비교
		if(member != null) {//정보가 존재하면 (null이 아니면)
			if(member.getMemberPw().equals(pw)) {//비밀번호가 일치하면
				//로그인완료->member의 정보를 자바 전역에 공유 위에다가 변수하나 만들거임
				System.out.println("정상 로그인 됐습니다");
				//로그인 성공한 member의 정보를 memberInfo에 넣음.
				memberInfo = member;
			}else {
				System.out.println("비밀번호가 틀립니다");
			}
		}else {//아이디가 존재하지않으면 (null이면) MemberDAO에서 그렇게 설정했음
			System.out.println("아이디가 존재하지 않습니다");
		}
	}
	//2.로그아웃      
	public void logout() {
		memberInfo = null;
		System.out.println("정상적으로 로그아웃 됐습니다.");
	}
	//회원조회
	public void getMemberList() {
		List<Member> list = MemberDAO.getInstance().getMemberList();
		for(int i=0; i<list.size(); i++) {
			//관리자와 사용자를 나누어 출력
			System.out.println("ID : "+list.get(i).getMemberId());
			System.out.println("PW : "+list.get(i).getMemberPw());
			System.out.println("PHONE : "+list.get(i).getMemberPhone());
			System.out.println("ADDR : "+list.get(i).getMemberAddr());
			//list안에 i 번째에 있는 member의 객체 필드값중에 GRADE를 대문자로 바꾼다음 
			//Y 와 비교해서 같으면 '관리자' 아니면 '사용자'
			if(list.get(i).getMemberGrade().toUpperCase().equals("N")) {
				System.out.println("GRADE : 관리자");
			}else {
				System.out.println("GRADE : 사용자");
			}
//			System.out.println(list.get(i).toString());
			System.out.println("========================================");
			
		}
	}
	//회원등록
	public void insertMember() {
		String id = "";
		String pw = "";
		String phone = "";
		String addr = "";
		String grade = "";
		Member member = new Member();
		//중복체크확인후 while 탈출
		while(true) {
			System.out.println("ID : ");
			id = sc.nextLine();
			member.setMemberId(id); //아이디 넣어서 없으면 null
			member = MemberDAO.getInstance().login(member);
			
			if(member == null) {
				member = new Member();//이거없으면 널포인트 익셉션 오류
				System.out.println("사용가능한 아이디입니다.");
				member.setMemberId(id);
				break;
			}else {
				System.out.println("이미 존재하는 아이디입니다 다시 입력 ㄱ");
			}
		}
		System.out.println("PW : ");
		pw = sc.nextLine();
		System.out.println("PHONE : ");
		phone = sc.nextLine();
		System.out.println("ADDR : ");
		addr = sc.nextLine();
		
		member.setMemberPw(pw);
		member.setMemberPhone(phone);
		member.setMemberAddr(addr);
		member.setMemberGrade("N");
		
		int result = MemberDAO.getInstance().insertMember(member);
		
		if(result == 1) {
			System.out.println("회원가입 됐습니다");
		}else if(result ==0) {
			System.out.println("회원가입이 안됐어요;");
		}
		
		
	}
	
	
	//회원삭제(탈퇴)
	public void deleteMember() {
		System.out.println("삭제 ID : ");
		String id = sc.nextLine();
		
		int result = MemberDAO.getInstance().deleteMember(id);
		
		if(result == 1) {
			System.out.println("아이디 삭제 완료");
		}else {
			System.out.println("아이디 삭제 실패");
		}
	}
	//회원수정 비밀번호
	public void updateMember() {
		//id pw 담을 객체 만드는것
		Member member = new Member();
		
		System.out.println("수정 비번 ㄱ : ");
		member.setMemberPw(sc.nextLine());//좋은곸드아니지만 일단 해봄
		System.out.println("비번바꿀 아이디 ㄱ:");
		member.setMemberId(sc.nextLine());
		
		int result = MemberDAO.getInstance().updateMember(member);
		
		if(result == 1) {
			System.out.println("정상수정 됐음 ㅋ");
			
		}else {
			System.out.println("수정 안됨;;;;;;");
		}
		
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
