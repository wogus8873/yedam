package com.yedam.member;

import java.util.ArrayList;
import java.util.List;

import com.yedma.common.DAO;

public class MemberDAO extends DAO{
	//로그인 , 회원 등록 , 회원 조회, 회원 탈퇴 ,  회원 수정 , 로그아웃
	//로그인 - login()
	//회원등록 - insertMember()
	//조회 - getMemberList()
	//탈퇴 - deleteMember()
	//수정 - updateMember()
	//로그아웃 - logout()
	
///////////////////////////////////////////////////////////////////   싱글톤
	private static MemberDAO memberDao = null;
	
	private MemberDAO() {
		
	}
	
	public static MemberDAO getInstance() {
		if(memberDao == null) {
			memberDao = new MemberDAO();
		}
		return memberDao;
	}
/////////////////////////////////////////////////////////////////////

	//1.로그인 id , pw 일치
	public Member login(Member member) {
		try {
			conn();
//			String sql = "select * from member where mem_id = ? AND mem_grade = n" 이렇게하면 관리자;
			String sql = "select * from member where mem_id = ?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, member.getMemberId());
			
			rs = pstmt.executeQuery();
			
			//id 는 primary key 라서 널이 있을 수 없다 따라서 아이디가 일치하지않으면 등록이 안 된것
			//정상적으로 회원정보 조회된 경우
			if(rs.next()) {
				member = new Member();
				member.setMemberId(rs.getString("mem_id"));
				member.setMemberPw(rs.getString("mem_pw"));
				member.setMemberPhone(rs.getString("mem_Phone"));
				member.setMemberAddr(rs.getString("mem_Addr"));
				member.setMemberGrade(rs.getString("mem_grade"));
			//회원의정보가 조회 안 된 경우
			}else {
				member = null;
				//조회할게없는데 객체만드는건 별로 그래서 null로 
			}
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			disconn();
		}
		return member;
	}
	
	
	
	//회원 조회
	public List<Member> getMemberList(){
		List<Member> list = new ArrayList<>();
		Member member = null;
		
		try {
			conn();
			String sql = "select * from member";
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			
			while(rs.next()) {
				member = new Member();
				member.setMemberId(rs.getString("mem_id"));
				member.setMemberPw(rs.getString("mem_pw"));
				member.setMemberPhone(rs.getString("mem_phone"));
				member.setMemberAddr(rs.getString("mem_addr"));
				member.setMemberGrade(rs.getString("mem_grade"));
				list.add(member);
			}
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			disconn();
		}
		return list;
	}
	
	
	//회원등록
	public int insertMember(Member member) {
		int result = 0;
		try {
			conn();
			
			//입력하는부분
			String sql = "insert into member values (?,?,?,?,?)";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1,member.getMemberId());
			pstmt.setString(2, member.getMemberPw());
			pstmt.setString(3,member.getMemberPhone());
			pstmt.setString(4, member.getMemberAddr());
			pstmt.setString(5, member.getMemberGrade());
			
			//입력해서 얻은값을 반환하는 부분
			result = pstmt.executeUpdate();
			
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			disconn();
		}
		
		
		
		return result;
	}
	
	//회원탈퇴
	public int deleteMember(String id) {
		int result = 0;
		try {
			conn();
			String sql = "delete from member where mem_id = ?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1,id);
			
			result = pstmt.executeUpdate();
			
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			disconn();
		}
		return result;
	}
	
	//회원수정 -  비밀번호만수정
	public int updateMember(Member member) {
		int result = 0;
		try {
			conn();
			String sql = "update member set mem_pw = ? where mem_id = ?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, member.getMemberPw());
			pstmt.setString(2, member.getMemberId());
			
			result = pstmt.executeUpdate();
			
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			disconn();
		}
		
		
		return result;
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
